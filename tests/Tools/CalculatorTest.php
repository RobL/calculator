<?php

require_once 'Tools/Calculator.php';

use Tools\Calculator;

/**
 * Calculator test case
 */
class CalculatorTest extends PHPUnit_Framework_TestCase
{
    private $calculator = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->calculator = new Calculator;
    }

    /**
     * Test adding together
     * @return void
     */
    public function testAddition()
    {
        $input = "1 + 1 + 3 + 7";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(12, $result);
    }

    /**
     * Test subtraction
     * @return void
     */
    public function testSubtraction()
    {
        $input = "12 - 3 - 4";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(5, $result);
    }

    /**
     * Test multiplication
     * @return void
     */
    public function testMultiplication()
    {
        $input = "3 * 4 * 5";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(60, $result);
    }

    /**
     * Test division
     * @return void
     */
    public function testDivision()
    {
        $input = "45 / 3 / 5";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(3, $result);
    }

    /**
     * Test the operators are executed in the correct order
     * @return void
     */
    public function testOrder()
    {
        $input = "3 + 4 - 6 * 2 / 3";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(3, $result);
    }

    /**
     * Test negative numbers
     * @return void
     */
    public function testNegative()
    {
        $input = "12 + -4";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(8, $result);
    }

    /**
     * Test floats
     * @return void
     */
    public function testFloat()
    {
        $input = "1.8 + 2.5";
        $result = $this->calculator->calculate($input);

        $this->assertEquals(4.3, $result);
    }
}
