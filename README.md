# DeskPRO Code Test
Rob Lowcock

## Usage
The unit tests can be run from the main directory with the following command, provided phpunit has been installed with `composer install` first

```
vendor/bin/phpunit tests/Tools/CalculatorTest.php
```

The main class is found in Tools/Calculator.php, and has one public function: calculate(), which expects one string input parameter. The class supports negative numbers and decimals.

## Limitations
Input *must* be formatted with spaces between the operators and numbers, in order to support negative numbers. The class recognises +, -, /, and * as operators, and expects a string such as "1 + 1 * 3 + 3"