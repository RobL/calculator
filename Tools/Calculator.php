<?php

namespace Tools;

/**
 * Tools\Calculator
 *
 * Calculator class for multiplication, division, addition and subtraction
 *
 * @category Tools
 * @package Tools
 * @subpackage Calculator
 * @author Rob Lowcock <rob.lowcock@gmail.com>
 */
class Calculator
{
    /**
     * Methods available
     * These will be executed in the order specified
     * @var array
     */
    private $methods = [
        'multiplication' => '*',
        'division' => '/',
        'addition' => '+',
        'subtraction' => '-',
    ];

    /**
     * Perform a calculation based on a string input
     * @param  string $input The original input
     * @return float         The result
     */
    public function calculate($input)
    {
        foreach ($this->methods as $method => $operator) {
            // Loop through to catch all additions
            // We can't use preg_match_all as that would mean overlapping capturing
            // groups
            while (strpos($input, ' ' . $operator . ' ') !== false) {
                $matches = [];

                // Regex to capture the numbers to be added
                preg_match('/(-?[0-9.]+) \\' . $operator . ' (-?[0-9.]+)/', $input, $matches);

                // Pass the matches to the relevant method
                $sum = $this->$method((float) $matches[1], (float) $matches[2]);

                // Replace the numbers with the total
                $input = str_replace(
                    $matches[1] . ' ' . $operator . ' ' . $matches[2],
                    (string) $sum, $input
                );
            }
        }

        return (float) $input;
    }

    /**
     * Multiply two inputs together
     * @param  int|float $input1 First input
     * @param  int|float $input2 Second input
     * @return float             Result
     */
    private function multiplication($input1, $input2)
    {
        return (float) ($input1 * $input2);
    }

    /**
     * Divide one input by another
     * @param  int|float $input1 The input to divide
     * @param  int|float $input2 The input to divide by
     * @return float             Result
     */
    private function division($input1, $input2)
    {
        return (float) ($input1 / $input2);
    }

    /**
     * Add two inputs together
     * @param  int|float $input1 First input
     * @param  int|float $input2 Second input
     * @return float             Result
     */
    private function addition($input1, $input2)
    {
        return (float) ($input1 + $input2);
    }

    /**
     * Subtract an input from another
     * @param  int|float $input1 The original input
     * @param  int|float $input2 The quantity to subtract
     * @return float             Result
     */
    private function subtraction($input1, $input2)
    {
        return (float) ($input1 - $input2);
    }
}
